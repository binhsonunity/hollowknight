using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyDoor : MonoBehaviour
{
    [SerializeField] private Key.KeyType keyType;
    [SerializeField] private GameObject winUI;

    private void Start()
    {
        winUI.SetActive(false);
    }
    public Key.KeyType GetKeyType()
    {
        return keyType;
    }
    public void OpenDoor()
    {
        gameObject.SetActive(false);
        winUI.SetActive(true);
        Time.timeScale = 0f;
    }
}
