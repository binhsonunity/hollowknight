using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaurseMenuUI : MonoBehaviour
{
    public GameObject panelPaurse;
    private void Start()
    {
        panelPaurse.SetActive(false);
    }
    public void Paurse()
    {
        panelPaurse.SetActive(true);
        Time.timeScale = 0;
    }
    public void Continue()
    {
        panelPaurse.SetActive(false);
        Time.timeScale = 1;
    }
}
