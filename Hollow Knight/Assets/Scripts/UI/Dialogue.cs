using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Dialogue : MonoBehaviour
{
    //Field
    //WindowChat
    public GameObject windowChat;
    //Indicator
    public GameObject indicator;
    //Dialogues List
    public List<string> dialogues;
    //Text Component
    public TMP_Text dialogueText;
    //Writing speed
    public float writingSpeed;
    //Index on dialogue
    private int index;
    //character index
    private int characterIndex;
    //Started Boolean
    private bool started;
    //Wait For Next boolean
    private bool waitForNext;

    private void Awake()
    {
        ToggleIndicator(false);
        ToggleWindow(false);
    }
    private void ToggleWindow(bool show)
    {
        windowChat.SetActive(show);
    }
    public void ToggleIndicator(bool show)
    {
        indicator.SetActive(show);
    }
    //Start Dialogue
    public void StartDialogue()
    {
        if (started)
        {
            return;
        }
        started = true;
        //show windowChat
        ToggleWindow(true);
        //hide indicator
        ToggleIndicator(false);
        //Start with first Dialogue
        GetDialogue(0);
    }
    private void GetDialogue(int i)
    {
        
        //start index at zero
        index = i;
        //Reset the character index
        characterIndex = 0;
        //clear the dialogue component text
        dialogueText.text = string.Empty;
        //Start Writing
        StartCoroutine(Writing());
    }
    //End Dialogue
    public void EndDialogue()
    {
        //Stared is disabled
        started = false;
        //Disable wait for next as well
        waitForNext = false;
        //Stop all Ienumerators
        StopAllCoroutines();
        //Hide the window
        ToggleWindow(false);
    }
    //Writing logic
    IEnumerator Writing()
    {
        yield return new WaitForSeconds(writingSpeed);
        string currentDialogue = dialogues[index];
        //Write the character
        dialogueText.text += currentDialogue[characterIndex];
        //increase the character index
        characterIndex++;
        //Make sure you have reached the end of the sentence
        if(characterIndex < currentDialogue.Length)
        {
            //Wait x seconds 
            yield return new WaitForSeconds(writingSpeed);
            //Restart the same process
            StartCoroutine(Writing());
        }
        else
        {
            //End this sentence and wait for the next one
            waitForNext = true;
        }
        
    }
    private void Update()
    {
        if (!started)
        {
            return;
        }
         if(waitForNext && Input.GetKeyDown(KeyCode.E))
        {
            waitForNext = false;
            index++;
            //Check if we are in the scope fo dialogues List 
            if(index < dialogues.Count)
            {
                //If so fetch the next dialogue
                GetDialogue(index);
            }
            else
            {
                //If not end the dialogue process
                ToggleIndicator(true);
                EndDialogue();
            }
        }
    }

}
