using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2_LookForPlayerState : LookForPlayerState
{
    private Enemy2 enemy2;
    public Enemy2_LookForPlayerState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_LookForPlayer data_LookForPlayer,Enemy2 enemy2) : base(entity, stateMachine, animBoolName, data_LookForPlayer)
    {
        this.enemy2 = enemy2;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isPlayerInMinAgroRange)
        {
            stateMachine.ChangeState(enemy2.playerDeteectedState);
        }
        else if (isAllTurnsTimeDone)
        {
            stateMachine.ChangeState(enemy2.moveState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
