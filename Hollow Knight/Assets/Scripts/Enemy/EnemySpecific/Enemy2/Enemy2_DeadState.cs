using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2_DeadState : DeadState
{
    private Enemy2 enemy2;
    public Enemy2_DeadState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_DeadState data_DeadState,Enemy2 enemy2) : base(entity, stateMachine, animBoolName, data_DeadState)
    {
        this.enemy2 = enemy2;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
