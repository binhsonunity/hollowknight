using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2_StunState : StunState
{
    private Enemy2 enemy2;
    public Enemy2_StunState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_StunState data_StunState,Enemy2 enemy2) : base(entity, stateMachine, animBoolName, data_StunState)
    {
        this.enemy2 = enemy2;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isStunTimeOver)
        {
            if (isPlayerInMinAgroRange)
            {
                stateMachine.ChangeState(enemy2.playerDeteectedState);
            }
            else
            {
                stateMachine.ChangeState(enemy2.lookForPlayerState);
            }
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
