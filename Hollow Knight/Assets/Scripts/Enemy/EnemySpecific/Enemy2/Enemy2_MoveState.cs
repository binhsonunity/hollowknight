using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2_MoveState : MoveState
{
    private Enemy2 enemy2;
    public Enemy2_MoveState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_MoveState stateData, Enemy2 enemy2) : base(entity, stateMachine, animBoolName, stateData)
    {
        this.enemy2 = enemy2;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isPlayerInMinAgroRange)
        {
            stateMachine.ChangeState(enemy2.playerDeteectedState);
        }
        else if (isDetectedWall || !isDetectedGround)
        {
            enemy2.idleState.SetFlipAfterIdle(true);
            stateMachine.ChangeState(enemy2.idleState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
