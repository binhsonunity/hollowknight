using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2_DodgeState : DodgeState
{
    private Enemy2 enemy2;
    public Enemy2_DodgeState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_DodgeState data_DodgeState,Enemy2 enemy2) : base(entity, stateMachine, animBoolName, data_DodgeState)
    {
        this.enemy2 = enemy2;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }
        
    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isDodgeOver)
        {
            if(isPlayerInMaxAgroRange && performCloseRangeAction)
            {
                stateMachine.ChangeState(enemy2.meleeAttackState);
            }
            else if (isPlayerInMaxAgroRange && !performCloseRangeAction)
            {
                stateMachine.ChangeState(enemy2.rangeAttackState);
            }
            else if (!isPlayerInMaxAgroRange)
            {
                stateMachine.ChangeState(enemy2.lookForPlayerState);
            }
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
