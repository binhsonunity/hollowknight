using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : Entity
{

    public Enemy2_MoveState moveState { get; private set; }
    public Enemy2_IdleState idleState { get; private set; }
    public Enemy2_PlayerDeteectedState playerDeteectedState { get; private set; }
    public Enemy2_MeleeAttackState meleeAttackState { get; private set; }
    public Enemy2_LookForPlayerState lookForPlayerState { get; private set; }
    public Enemy2_StunState stunState { get; private set; }
    public Enemy2_DeadState deadState { get; private set; }
    public Enemy2_DodgeState dodgeState { get; private set; }
    public Enemy2_RangeAttackState rangeAttackState { get; private set; }

    [SerializeField] private Data_MoveState data_MoveState;
    [SerializeField] private Data_IdleState_ data_IdleState;
    [SerializeField] private Data_PlayerDetected data_PlayerDetected;
    [SerializeField] private Data_MeleeAttackState data_MeleeAttack;
    [SerializeField] private Data_LookForPlayer data_LookForPlayer;
    [SerializeField] private Data_StunState data_StunState;
    [SerializeField] private Data_DeadState data_DeadState;
    [SerializeField] public Data_DodgeState data_DodgeState;
    [SerializeField] private Data_RangeAttackState data_RangeAttack;

    [SerializeField] private Transform meleeAttackPosition;
    [SerializeField] private Transform rangedAttackPosition;
    
 
    public override void Start()
    {
        base.Start();
        moveState = new Enemy2_MoveState(this, stateMachine, "move", data_MoveState, this);
        idleState = new Enemy2_IdleState(this, stateMachine, "idle", data_IdleState, this);
        playerDeteectedState = new Enemy2_PlayerDeteectedState(this, stateMachine, "playerDetected", data_PlayerDetected, this);
        meleeAttackState = new Enemy2_MeleeAttackState(this, stateMachine, "meleeAttack", meleeAttackPosition,data_MeleeAttack, this);
        lookForPlayerState = new Enemy2_LookForPlayerState(this, stateMachine, "lookForPlayer", data_LookForPlayer, this);
        stunState = new Enemy2_StunState(this, stateMachine, "stunState", data_StunState, this);
        deadState = new Enemy2_DeadState(this, stateMachine, "dead", data_DeadState, this);
        dodgeState = new Enemy2_DodgeState(this, stateMachine, "dodge", data_DodgeState, this);
        rangeAttackState = new Enemy2_RangeAttackState(this, stateMachine, "rangedAttack", rangedAttackPosition,data_RangeAttack, this);
        stateMachine.Initialize(moveState);

    }

    public override void Damage(AttackDetails attackDetails)
    {
        base.Damage(attackDetails);
        if (isDead)
        {
            stateMachine.ChangeState(deadState);
        }
        else if (isStunned && stateMachine.currentState != stunState )
        {
            stateMachine.ChangeState(stunState);
        }
        else if (CheckPlayerInMinAgroRange())
        {
            stateMachine.ChangeState(rangeAttackState);
        }
        else if (!CheckPlayerInMaxAgroRange())
        {
            lookForPlayerState.SetTurnImmediately(true);
            stateMachine.ChangeState(lookForPlayerState);
        }
    }

    public override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
        Gizmos.DrawWireSphere(meleeAttackPosition.position, data_MeleeAttack.attackRadius);
    }
}
