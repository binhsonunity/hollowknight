using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2_PlayerDeteectedState : PlayerDetectedState
{
    private Enemy2 enemy2;
    public Enemy2_PlayerDeteectedState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_PlayerDetected data_PlayerDetected,Enemy2 enemy2) : base(entity, stateMachine, animBoolName, data_PlayerDetected)
    {
        this.enemy2 = enemy2;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (performCloseRangeAction)
        {
            if(Time.time >= enemy2.dodgeState.startTime+ enemy2.data_DodgeState.dodgeCooldown)
            {
                stateMachine.ChangeState(enemy2.dodgeState);
            }
            else
            {
                stateMachine.ChangeState(enemy2.meleeAttackState);
            }             
        }
        else if (performLongRangeAction)
        {
            stateMachine.ChangeState(enemy2.rangeAttackState);
        }
        else if (!isPlayerInMaxAgroRange)
        {
            stateMachine.ChangeState(enemy2.lookForPlayerState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
