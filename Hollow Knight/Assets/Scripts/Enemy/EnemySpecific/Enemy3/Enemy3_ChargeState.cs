using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3_ChargeState : ChargeState
{
    private Enemy3 enemy3;
    public Enemy3_ChargeState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_ChargeState data_ChargeState,Enemy3 enemy3) : base(entity, stateMachine, animBoolName, data_ChargeState)
    {
        this.enemy3 = enemy3;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (performCloseRangeAction)
        {
            stateMachine.ChangeState(enemy3.meleeAttackState);
        }
        else if (!isDetectingGround || isDetectingWall)
        {
            stateMachine.ChangeState(enemy3.lookForPlayerState);
        }
        else if (isChargeTimeOver)
        {
            if (isPlayerInMinAgroRange)
            {
                stateMachine.ChangeState(enemy3.playerDetectedState);
            }
            else
            {
                stateMachine.ChangeState(enemy3.lookForPlayerState);
            }
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
