using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3_DeadState : DeadState
{
    private Enemy3 enemy3;
    public Enemy3_DeadState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_DeadState data_DeadState,Enemy3 enemy3) : base(entity, stateMachine, animBoolName, data_DeadState)
    {
        this.enemy3 = enemy3;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
