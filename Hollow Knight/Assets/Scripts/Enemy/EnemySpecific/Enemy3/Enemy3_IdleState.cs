using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3_IdleState : IdleState
{
    private Enemy3 enemy3;
    public Enemy3_IdleState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_IdleState_ data_IdleState,Enemy3 enemy3) : base(entity, stateMachine, animBoolName, data_IdleState)
    {
        this.enemy3 = enemy3;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isPlayerInMinAgroRange)
        {
            stateMachine.ChangeState(enemy3.playerDetectedState);
        }
        else if (isIdleTimeOver)
        {
            stateMachine.ChangeState(enemy3.moveState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
