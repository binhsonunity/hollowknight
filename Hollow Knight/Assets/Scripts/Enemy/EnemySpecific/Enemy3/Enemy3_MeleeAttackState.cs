using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3_MeleeAttackState : MeleeAttackState
{
    private Enemy3 enemy3;
    public Enemy3_MeleeAttackState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Transform attackPosition, Data_MeleeAttackState data_MeleeAttackState,Enemy3 enemy3) : base(entity, stateMachine, animBoolName, attackPosition, data_MeleeAttackState)
    {
        this.enemy3 = enemy3;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void FinishAttack()
    {
        base.FinishAttack();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isAnimationFinished)
        {
            if (isPlayerInMinAgroRange)
            {
                stateMachine.ChangeState(enemy3.playerDetectedState);
            }
            else
            {
                stateMachine.ChangeState(enemy3.lookForPlayerState);
            }
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void TriggerAttack()
    {
        base.TriggerAttack();
    }
}
