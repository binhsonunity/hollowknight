using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3_StunState : StunState
{
    private Enemy3 enemy3;
    public Enemy3_StunState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_StunState data_StunState,Enemy3 enemy3) : base(entity, stateMachine, animBoolName, data_StunState)
    {
        this.enemy3 = enemy3;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isStunTimeOver)
        {
            if (performCloseRangeAction)
            {
                stateMachine.ChangeState(enemy3.meleeAttackState);
            }
            else if (isPlayerInMinAgroRange)
            {
                stateMachine.ChangeState(enemy3.chargeState);
            }
            else
            {
                enemy3.lookForPlayerState.SetTurnImmediately(true);
                stateMachine.ChangeState(enemy3.lookForPlayerState);
            }
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
