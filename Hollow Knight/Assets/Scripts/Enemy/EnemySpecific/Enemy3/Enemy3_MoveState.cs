using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3_MoveState : MoveState
{
    private Enemy3 enemy3;
    public Enemy3_MoveState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_MoveState stateData,Enemy3 enemy3) : base(entity, stateMachine, animBoolName, stateData)
    {
        this.enemy3 = enemy3;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isPlayerInMinAgroRange)
        {
            stateMachine.ChangeState(enemy3.playerDetectedState);
        }
        else if (isDetectedWall || !isDetectedGround)
        {
            //TODO:add transition to idle
            enemy3.idleState.SetFlipAfterIdle(true);
            stateMachine.ChangeState(enemy3.idleState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
