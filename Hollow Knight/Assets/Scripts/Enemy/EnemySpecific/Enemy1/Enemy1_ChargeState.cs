using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1_ChargeState : ChargeState
{
    private Enemy1 enemy1;

    public Enemy1_ChargeState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_ChargeState data_ChargeState,Enemy1 enemy1) : base(entity, stateMachine, animBoolName, data_ChargeState)
    {
        this.enemy1 = enemy1;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (performCloseRangeAction)
        {
            stateMachine.ChangeState(enemy1.meleeAttackState);
        }
        else if (!isDetectingGround || isDetectingWall)
        {
            stateMachine.ChangeState(enemy1.lookForPlayerState);
        }
        else if (isChargeTimeOver)
        {
            if (isPlayerInMinAgroRange)
            {
                stateMachine.ChangeState(enemy1.playerDetectedState);
            }
            else
            {
                stateMachine.ChangeState(enemy1.lookForPlayerState);
            }
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
