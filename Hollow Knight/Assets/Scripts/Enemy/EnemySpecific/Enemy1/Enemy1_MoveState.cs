using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1_MoveState : MoveState
{
    private Enemy1 enemy1;
    public Enemy1_MoveState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_MoveState stateData,Enemy1 enemy1) : base(entity, stateMachine, animBoolName, stateData)
    {
        this.enemy1 = enemy1;
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isPlayerInMinAgroRange)
        {
            stateMachine.ChangeState(enemy1.playerDetectedState);
        }
        else if (isDetectedWall || !isDetectedGround)
        {
            //TODO:add transition to idle
            enemy1.idleState.SetFlipAfterIdle(true);
            stateMachine.ChangeState(enemy1.idleState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
