using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1_StunState : StunState
{
    private Enemy1 enemy1;
    public Enemy1_StunState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_StunState data_StunState,Enemy1 enemy1) : base(entity, stateMachine, animBoolName, data_StunState)
    {
        this.enemy1 = enemy1;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isStunTimeOver)
        {
            if (performCloseRangeAction)
            {
                stateMachine.ChangeState(enemy1.meleeAttackState);
            }
            else if (isPlayerInMinAgroRange)
            {
                stateMachine.ChangeState(enemy1.chargeState);
            }
            else
            {
                enemy1.lookForPlayerState.SetTurnImmediately(true);
                stateMachine.ChangeState(enemy1.lookForPlayerState);
            }
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }   
}
