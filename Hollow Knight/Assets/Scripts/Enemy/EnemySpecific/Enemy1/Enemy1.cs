using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1 : Entity
{

    public Enemy1_IdleState idleState { get; private set; }
    public Enemy1_MoveState moveState { get; private set; }
    public Enemy1_PlayerDetected playerDetectedState { get; private set; }
    public Enemy1_ChargeState chargeState { get; private set; }
    public Enemy1_LookForPlayerState lookForPlayerState { get; private set; }
    public Enemy1_MeleeAttackState meleeAttackState { get; private set; }
    public Enemy1_StunState stunState { get; private set; }
    public Enemy1_DeadState deadState { get; private set; }

    

    [SerializeField] private Data_IdleState_ data_IdleState;
    [SerializeField] private Data_MoveState data_MoveState;
    [SerializeField] private Data_PlayerDetected data_PlayerDetected;
    [SerializeField] private Data_ChargeState data_ChargeState;
    [SerializeField] private Data_LookForPlayer data_LookForPlayerState;
    [SerializeField] private Data_MeleeAttackState data_MeleeAttackState;
    [SerializeField] private Data_StunState data_StunState;
    [SerializeField] private Data_DeadState data_DeadState;

    [SerializeField] private Transform meleeAttackPosition;

    public override void Start()
    {
        base.Start();
        moveState = new Enemy1_MoveState(this, stateMachine, "move", data_MoveState, this);
        idleState = new Enemy1_IdleState(this, stateMachine, "idle", data_IdleState, this);
        playerDetectedState = new Enemy1_PlayerDetected(this, stateMachine, "playerDetected",data_PlayerDetected,this);
        chargeState = new Enemy1_ChargeState(this, stateMachine, "charge", data_ChargeState, this);
        lookForPlayerState = new Enemy1_LookForPlayerState(this, stateMachine, "lookForPlayer", data_LookForPlayerState, this);
        meleeAttackState = new Enemy1_MeleeAttackState(this, stateMachine, "meleeAttackPlayer",meleeAttackPosition ,data_MeleeAttackState, this);
        stunState = new Enemy1_StunState(this, stateMachine, "stun", data_StunState, this);
        deadState = new Enemy1_DeadState(this, stateMachine, "dead", data_DeadState, this);
        

        stateMachine.Initialize(moveState);
    }

    public override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
        Gizmos.DrawWireSphere(meleeAttackPosition.position, data_MeleeAttackState.attackRadius);
    }

    public override void Damage(AttackDetails attackDetails)
    {
        base.Damage(attackDetails); 
        if (isDead)
        {
            stateMachine.ChangeState(deadState);
        }
        else if (isStunned && stateMachine.currentState != stunState)
        {
            stateMachine.ChangeState(stunState);
        }
       
    }

}
