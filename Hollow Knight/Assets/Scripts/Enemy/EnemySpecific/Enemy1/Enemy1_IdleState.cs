using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1_IdleState : IdleState
{
    private Enemy1 enemy1;

    public Enemy1_IdleState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_IdleState_ data_IdleState,Enemy1 enemy1) : base(entity, stateMachine, animBoolName, data_IdleState)
    {
        this.enemy1 = enemy1;
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isPlayerInMinAgroRange)
        {
            stateMachine.ChangeState(enemy1.playerDetectedState);
        }
        else if (isIdleTimeOver)
        {
            stateMachine.ChangeState(enemy1.moveState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
