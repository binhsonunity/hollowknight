using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1_PlayerDetected : PlayerDetectedState
{
    private Enemy1 enemy1;
    public Enemy1_PlayerDetected(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_PlayerDetected data_PlayerDetected,Enemy1 enemy1) : base(entity, stateMachine, animBoolName, data_PlayerDetected)
    {
        this.enemy1 = enemy1;
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (performCloseRangeAction)
        {
            stateMachine.ChangeState(enemy1.meleeAttackState);
        }
        else if (performLongRangeAction)
        {
            stateMachine.ChangeState(enemy1.chargeState);
        }
        else if (!isPlayerInMaxAgroRange)
        {
            stateMachine.ChangeState(enemy1.lookForPlayerState);
        }
        else if (!isDetectingGround)
        {
            entity.Flip();
            stateMachine.ChangeState(enemy1.moveState);
        }


    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
