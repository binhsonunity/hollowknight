using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1_DeadState : DeadState
{
    private Enemy1 enemy1;
    public Enemy1_DeadState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Data_DeadState data_DeadState,Enemy1 enemy1) : base(entity, stateMachine, animBoolName, data_DeadState)
    {
        this.enemy1 = enemy1;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
