using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunState : State
{
    protected Data_StunState data_StunState;
    protected bool isStunTimeOver;
    protected bool isStunGround;
    protected bool isMovementStopped;
    protected bool performCloseRangeAction;
    protected bool isPlayerInMinAgroRange;
    public StunState(Entity entity, FiniteStateMachine stateMachine, string animBoolName,Data_StunState data_StunState) : base(entity, stateMachine, animBoolName)
    {
        this.data_StunState = data_StunState;
    }

    public override void DoChecks()
    {
        base.DoChecks();
        isStunGround = entity.CheckStunGround();
        performCloseRangeAction = entity.CheckPlayerInCloseRangeAction();
        isPlayerInMinAgroRange = entity.CheckPlayerInMinAgroRange();
    }

    public override void Enter()
    {
        base.Enter();
        isStunTimeOver = false;
        isMovementStopped = false;
        entity.Setvelocity(data_StunState.stunKnockBackSpeed, data_StunState.stunKnockBackAngle, entity.lastDamageDirection);
    }

    public override void Exit()
    {
        base.Exit();
        entity.ResetStunResistance();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if(Time.time >= startTime + data_StunState.stunTime)
        {
            isStunTimeOver = true;
        }
        if (isStunGround && Time.time >= startTime + data_StunState.stunKnockBackTime && !isMovementStopped)
        {
            isMovementStopped = true;
            entity.SetVelocity(0f);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
