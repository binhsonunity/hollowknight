using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadState : State
{
    protected Data_DeadState data_DeadState;
    public DeadState(Entity entity, FiniteStateMachine stateMachine, string animBoolName,Data_DeadState data_DeadState) : base(entity, stateMachine, animBoolName)
    {
        this.data_DeadState = data_DeadState;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
        GameObject.Instantiate(data_DeadState.deathBloodParticle, entity.aliveGO.transform.position, data_DeadState.deathBloodParticle.transform.rotation);
        GameObject.Instantiate(data_DeadState.deathChunkParticle, entity.aliveGO.transform.position, data_DeadState.deathChunkParticle.transform.rotation);
        
        entity.gameObject.SetActive(false);
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
