using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newMeleeAttackStateData", menuName = "Data/State Data/Melee Attack State")]
public class Data_MeleeAttackState : ScriptableObject
{
    public float attackRadius = 0.5f;
    public int attackDamage = 10;
    public LayerMask whatIsPlayer;
}
