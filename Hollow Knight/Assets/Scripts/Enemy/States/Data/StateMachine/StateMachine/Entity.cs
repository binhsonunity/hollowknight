using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Entity : MonoBehaviour
{
    public FiniteStateMachine stateMachine;
    public Data_Enity enityData;
    public int facingDirection { get; private set; }
    public Rigidbody2D rb2D { get; private set; }
    public Animator anim { get; private set; }
    public GameObject aliveGO { get; private set; }

    public AnimationToStateMachine animationToStateMachine { get; private set; }
    public int lastDamageDirection { get; private set; }

    private Vector2 velocityWorkspace;
    [SerializeField] private Transform wallCheck;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private Transform stunGroundCheck;
    [SerializeField] private Transform playerCheck;
    

    private float currentHealth;
    private float currentStunResistance;
    private float lastDamageTime;

    protected bool isStunned;
    protected bool isDead;



    public virtual void Start()
    {
        facingDirection = 1;
        aliveGO = transform.Find("Alive").gameObject;
        rb2D = aliveGO.GetComponent<Rigidbody2D>();
        anim = aliveGO.GetComponent<Animator>();
        animationToStateMachine = aliveGO.GetComponent<AnimationToStateMachine>();

        stateMachine = new FiniteStateMachine();
        currentHealth = enityData.maxHealth;
        currentStunResistance = enityData.stunResistance;
        
    }

    public virtual void Update()
    {
        stateMachine.currentState.LogicUpdate();
        anim.SetFloat("yVelocity", rb2D.velocity.y);
        if(Time.time >= lastDamageTime + enityData.stunRecoveryTime)
        {
            ResetStunResistance();
        }
    }
    public virtual void FixedUpdate()
    {
        stateMachine.currentState.PhysicsUpdate();
    }
    public virtual void SetVelocity(float velocity)
    {
        velocityWorkspace.Set(facingDirection * velocity,rb2D.velocity.y);
        rb2D.velocity = velocityWorkspace;
    }

    public virtual bool CheckWall()
    {
        return Physics2D.Raycast(wallCheck.position, aliveGO.transform.right, enityData.wallCheckDistance, enityData.whatIsGround);
        
    }

    public virtual bool CheckGround()
    {
        return Physics2D.Raycast(groundCheck.position, Vector2.down, enityData.groundCheckDistance, enityData.whatIsGround);
    }
    public virtual bool CheckStunGround()
    {
        return Physics2D.OverlapCircle(stunGroundCheck.position, enityData.stunGroundRadius, enityData.whatIsGround);
    }

    public virtual bool CheckPlayerInMinAgroRange()
    {
        return Physics2D.Raycast(playerCheck.position, aliveGO.transform.right, enityData.minAgroDistance, enityData.whatIsPlayer);
    }

    public virtual bool CheckPlayerInMaxAgroRange()
    {
        return Physics2D.Raycast(playerCheck.position, aliveGO.transform.right, enityData.maxAgroDistance, enityData.whatIsPlayer);
    }
    public virtual void DamageHop(float velocity)
    {
        velocityWorkspace.Set(rb2D.velocity.x, velocity);
        rb2D.velocity = velocityWorkspace;
    }

    public virtual void Setvelocity(float velocity,Vector2 angle,int direction)
    {
        angle.Normalize();
        velocityWorkspace.Set(angle.x * velocity * direction, angle.y * velocity);
        rb2D.velocity = velocityWorkspace;
    }
    public virtual void ResetStunResistance()
    {
        isStunned = false;
        currentStunResistance = enityData.stunResistance;
    }
    public virtual void Damage(AttackDetails attackDetails)
    {
        lastDamageTime = Time.time;
        currentHealth -= attackDetails.damageAmount;
        currentStunResistance -= attackDetails.stunDamageAmount;
        DamageHop(enityData.damageHopSpeed);

        Instantiate(enityData.hitParticle, aliveGO.transform.position, Quaternion.Euler(0f, 0f, Random.Range(0f, 360f)));

        if(attackDetails.position.x > aliveGO.transform.position.x)
        {
            lastDamageDirection = -1; 
        }
        else
        {
            lastDamageDirection = 1;
        }

        if(currentStunResistance <= 0)
        {
            isStunned = true;
        }
        if(currentHealth <= 0)
        {
            isDead = true;
        }
    }
    public virtual bool CheckPlayerInCloseRangeAction()
    {
        return Physics2D.Raycast(playerCheck.position, aliveGO.transform.right, enityData.closeRangeActionDistance, enityData.whatIsPlayer);
    }
    public virtual void Flip()
    {
        facingDirection *= -1;
        aliveGO.transform.Rotate(0.0f, 180.0f, 0f);
    }  

    public virtual void OnDrawGizmos()
    {
        Gizmos.DrawLine(wallCheck.position, wallCheck.position + (Vector3)(Vector2.right * facingDirection * enityData.wallCheckDistance));
        Gizmos.DrawLine(groundCheck.position, groundCheck.position + (Vector3)(Vector2.down * enityData.wallCheckDistance));

        Gizmos.DrawWireSphere((playerCheck.position + (Vector3)(Vector2.right * enityData.closeRangeActionDistance)), 0.2f);
        Gizmos.DrawWireSphere((playerCheck.position + (Vector3)(Vector2.right * enityData.minAgroDistance)), 0.2f);
        Gizmos.DrawWireSphere((playerCheck.position + (Vector3)(Vector2.right * enityData.maxAgroDistance)), 0.2f);
    }
}
