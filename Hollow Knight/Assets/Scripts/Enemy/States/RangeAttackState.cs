using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeAttackState : AttackState
{
    protected Data_RangeAttackState data_RangeAttack;
    protected GameObject projectile;
    protected Projectiles projectilesScript;

    public RangeAttackState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Transform attackPosition,Data_RangeAttackState data_RangeAttackState) : base(entity, stateMachine, animBoolName, attackPosition)
    {
        this.data_RangeAttack = data_RangeAttackState;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void FinishAttack()
    {
        base.FinishAttack();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void TriggerAttack()
    {
        base.TriggerAttack();
        projectile = GameObject.Instantiate(data_RangeAttack.projectile, attackPosition.position, attackPosition.rotation);
        projectilesScript = projectile.GetComponent<Projectiles>();
        projectilesScript.FireProjectile(data_RangeAttack.projectileSpeed, data_RangeAttack.projectileTravelDistance, data_RangeAttack.projectileDamage);
    }
}
