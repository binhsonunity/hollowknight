using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttackState : AttackState
{
    protected Data_MeleeAttackState data_MeleeAttackState;
    protected AttackDetails attackDetails;
    public MeleeAttackState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Transform attackPosition,Data_MeleeAttackState data_MeleeAttackState) : base(entity, stateMachine, animBoolName, attackPosition)
    {
        this.data_MeleeAttackState = data_MeleeAttackState;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
        attackDetails.damageAmount = data_MeleeAttackState.attackDamage;
        attackDetails.position = entity.aliveGO.transform.position;
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void FinishAttack()
    {
        base.FinishAttack();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void TriggerAttack()
    {
        base.TriggerAttack();
        Collider2D[] detectedObject = Physics2D.OverlapCircleAll(attackPosition.position, data_MeleeAttackState.attackRadius, data_MeleeAttackState.whatIsPlayer);
        foreach(Collider2D collider in detectedObject)
        {
            collider.transform.SendMessage("Damage", attackDetails);
        }
    }
}
