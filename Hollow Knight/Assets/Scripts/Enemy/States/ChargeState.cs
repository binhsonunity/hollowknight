using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeState : State
{

    protected Data_ChargeState data_ChargeState;
    protected bool isPlayerInMinAgroRange;
    protected bool isDetectingGround;
    protected bool isDetectingWall;
    protected bool isChargeTimeOver;
    protected bool performCloseRangeAction;
    public ChargeState(Entity entity, FiniteStateMachine stateMachine, string animBoolName,Data_ChargeState data_ChargeState) : base(entity, stateMachine, animBoolName)
    {
        this.data_ChargeState = data_ChargeState;
    }

    public override void DoChecks()
    {
        base.DoChecks();
        isPlayerInMinAgroRange = entity.CheckPlayerInMinAgroRange();
        isDetectingGround = entity.CheckGround();
        isDetectingWall = entity.CheckWall();

        performCloseRangeAction = entity.CheckPlayerInCloseRangeAction();
    }

    public override void Enter()
    {
        base.Enter();
        isChargeTimeOver = false;
        entity.SetVelocity(data_ChargeState.chargeSpeed);
        
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if(Time.time >= startTime + data_ChargeState.chargeTime)
        {
            isChargeTimeOver = true;
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
        
    }
}
