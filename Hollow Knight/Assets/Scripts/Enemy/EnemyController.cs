using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private enum State
    {
        Moving,
        Knockback,
        Dead
    }

    private State currentState;
    [SerializeField] private float groundCheckDistance, wallCheckDistance,moveSpeed,maxHealth,knockbackDuration,lastTouchDamageTime,touchDamageCoolDown,
        touchDamage,touchDamageWidth,touchDamageHeight;
    [SerializeField] private Transform groudCheck, wallCheck,touchDamageCheck;
    [SerializeField] private LayerMask whatIsGround,whatIsPlayer;
    [SerializeField] private Vector2 knockbackSpeed;
    [SerializeField] private GameObject hitParticle, deathChunkParticle,deathBloodParticle;
    private int facingDirection,damageDirection;
    private bool groundDetected, wallDetected;
    private GameObject alive;
    private Rigidbody2D aliveRb2d;
    private Animator aliveAnim;
    private float currentHealth,knockbackStarTime;
    private float[] attackDetails = new float[2];
    private Vector2 movement,touchDamageBotLeft,touchDamageTopRight;

    private void Start()
    {
        alive = transform.Find("Alive").gameObject;
        aliveRb2d = alive.GetComponent<Rigidbody2D>();
        aliveAnim = alive.GetComponent<Animator>();
        currentHealth = maxHealth;
        facingDirection = 1;
        
    }

    private void Update()
    {
        switch (currentState)
        {
            case State.Moving:
                UpdateMovingState();
                break;
            case State.Knockback:
                UpdateKnockBackState();
                break;
            case State.Dead:
                UpdateDeadState();
                break;
            
        }
    }

    //Walking State
    private void EnterMovingState()
    {

    }

    private void UpdateMovingState()
    {
        groundDetected = Physics2D.Raycast(groudCheck.position, Vector2.down, groundCheckDistance, whatIsGround);
        wallDetected = Physics2D.Raycast(wallCheck.position, transform.right, wallCheckDistance, whatIsGround);
        CheckTouchDamage();
        if(!groundDetected || wallDetected)
        {
            //flip
            Flip();
        }
        else
        {
            //Move
            movement.Set(moveSpeed * facingDirection, aliveRb2d.velocity.y);
            aliveRb2d.velocity = movement;
        }
    }

    private void ExitMovingState()
    {

    }

    //Knockback State
    private void EnterKnockBackState()
    {
        knockbackStarTime = Time.time;
        movement.Set(knockbackSpeed.x * damageDirection, knockbackSpeed.y);
        aliveRb2d.velocity = movement;
        aliveAnim.SetBool("Knockback", true);
    }

    private void UpdateKnockBackState()
    {
        if(Time.time >= knockbackStarTime + knockbackDuration)
        {
            SwitchState(State.Moving);
        }
    }

    private void ExitKnockBackState()
    {
        aliveAnim.SetBool("Knockback", false);
    }

    //Dead State
    private void EnterDeadState()
    {
        Instantiate(deathChunkParticle, alive.transform.position, deathChunkParticle.transform.rotation);
        Instantiate(deathBloodParticle, alive.transform.position, deathBloodParticle.transform.rotation);
        Destroy(gameObject);
    }
    private void UpdateDeadState()
    {
        //Spawn chunks and blood
        Destroy(gameObject);
    }
    private void ExitDeadState()
    {

    }

    //Other Functions
    private void Damage(float[] attackDetails) 
    {
        currentHealth -= attackDetails[0];

        Instantiate(hitParticle, alive.transform.position, Quaternion.Euler(0.0f,0.0f,Random.Range(0.0f,360.0f)));

        if(attackDetails[1] > alive.transform.position.x)
        {
            damageDirection = -1;
        }
        else
        {
            damageDirection = 1;
        }

        //Hit particle
        if(currentHealth > 0.0f)
        {
            SwitchState(State.Knockback);
        }
        else if(currentHealth <= 0.0f)
        {
            SwitchState(State.Dead);
        }

    }

    private void CheckTouchDamage()
    {
        if(Time.time >= lastTouchDamageTime + touchDamageCoolDown)
        {
            touchDamageBotLeft.Set(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeight / 2));
            touchDamageTopRight.Set(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeight / 2));

            Collider2D hit = Physics2D.OverlapArea(touchDamageBotLeft, touchDamageTopRight,whatIsPlayer);

            if(hit != null)
            {
                lastTouchDamageTime = Time.time;
                attackDetails[0] = touchDamage;
                attackDetails[1] = alive.transform.position.x;
                hit.SendMessage("Damage", attackDetails);

            }
        }
    }
    private void Flip()
    {
        facingDirection *= -1;
        alive.transform.Rotate(0.0f, 180.0f, 0.0f);
    }
    private void SwitchState(State state)
    {
        switch (currentState)
        {
            case State.Moving:
                ExitMovingState();
                break;
            case State.Knockback:
                ExitKnockBackState();
                break;
            case State.Dead:
                ExitDeadState();
                break;
        }

        switch (state)
        {
            case State.Moving:
                EnterMovingState();
                break;
            case State.Knockback:
                EnterKnockBackState();
                break;
            case State.Dead:
                EnterDeadState();
                break;
        }

        currentState = state;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(groudCheck.position, new Vector2(groudCheck.position.x, groudCheck.position.y - groundCheckDistance));
        Gizmos.DrawLine(wallCheck.position, new Vector2(wallCheck.position.x + wallCheckDistance, wallCheck.position.y));

        Vector2 botLeft = new Vector2(touchDamageCheck.position.x -(touchDamageWidth/2),touchDamageCheck.position.y - (touchDamageHeight /2));
        Vector2 botRight = new Vector2(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeight / 2));
        Vector2 topRight = new Vector2(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeight / 2));
        Vector2 topLeft = new Vector2(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeight / 2));

        Gizmos.DrawLine(botLeft, botRight);
        Gizmos.DrawLine(botRight, topRight);
        Gizmos.DrawLine(topRight, topLeft);
        Gizmos.DrawLine(topLeft, botLeft);
    }
}
