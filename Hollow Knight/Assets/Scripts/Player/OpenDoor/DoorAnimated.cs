using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnimated : MonoBehaviour,IDoor
{
    private bool isOpen = false;
    private Animator anim;
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    public void OpenDoor()
    {
        isOpen = true;
        anim.SetBool("Open",true);
    }
    public void CloseDoor()
    {
        isOpen = false;
        anim.SetBool("Open", false);
    }

    public void ToggleDoor()
    {
        isOpen = !isOpen;
        if (isOpen)
        {
            OpenDoor();
        }
        else
        {
            CloseDoor();
        }
    }
}
