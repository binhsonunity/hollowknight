using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteractPressurePlate : MonoBehaviour
{
    [SerializeField] private GameObject doorGameObject;
    private IDoor door;
    private float timer;
    private Animator anim;

    private void Awake()
    {
        door = doorGameObject.GetComponent<IDoor>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if(timer > 0)
        {
            timer -= Time.deltaTime;
            if(timer <= 0f)
            {
                door.CloseDoor();
                anim.SetBool("OpenLever", false);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.GetComponent<PlayerController>() != null)
        {     
                door.OpenDoor();
                anim.SetBool("OpenLever", true);                                         
        }
    }
    private void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.GetComponent<PlayerController>() != null)
        {
            timer = 5f;         
        }
    }   
}
