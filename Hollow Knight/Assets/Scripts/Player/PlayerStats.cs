using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    [SerializeField] private int maxHelath;
    [SerializeField] private GameObject deathChunkParticle,deathBloodParticle; 
    public float currentHealth { get; private set; }
    private GameManager gameManager;
    BoxCollider2D myBoxCollider2D;
   
            
    private void Awake()
    {
        
    }


    private void Start()
    {
        //CallHealthEvent(0);
        currentHealth = maxHelath;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        myBoxCollider2D = GetComponent<BoxCollider2D>();
        
    }
    private void Update()
    {
        Check();
        if (Input.GetKeyDown(KeyCode.R))
        {
            DecreaseHeakth(1);
        }
    }
    public void DecreaseHeakth(float amount)
    {
        currentHealth -= amount;
        
        if(currentHealth <= 0.0f)
        {
            Die();
        }
    }

    private void Die()
    {
        Instantiate(deathChunkParticle, transform.position, transform.rotation);
        Instantiate(deathBloodParticle, transform.position, transform.rotation);
        gameManager.Respawn();
        Destroy(gameObject);       
    }
    
    private void Check() 
    {
        if (myBoxCollider2D.IsTouchingLayers(LayerMask.GetMask("Obstacle")))
        {
            Die();
        }
    }

    /*
    public void AddHealth(int healthPercent)
    {
        int healthIncrease = Mathf.RoundToInt((maxHelath * healthPercent) / 10f);
        int totalHealth = currentHealth + healthIncrease;
        if(totalHealth> maxHelath)
        {
            currentHealth = maxHelath;
        }
        else
        {
            currentHealth = totalHealth;
        }
        CallHealthEvent(0);
    }
    private void CallHealthEvent(int damageAmount)
    {
        // Trigger health event
        healthEvent.CallHealthChangedEvent(((float)currentHealth / (float)maxHelath), currentHealth, damageAmount);
    }
    */
}
