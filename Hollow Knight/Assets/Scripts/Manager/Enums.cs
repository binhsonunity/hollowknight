public enum GameState
{
    gameStart,
    gameWon,
    gamePaused,
    gameLost,
}